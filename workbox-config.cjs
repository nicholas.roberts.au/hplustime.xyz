module.exports = {
  "globDirectory": "_site/",
  "globPatterns": [
    "**/*.{js,ico,css,html,json}"
  ],
  "swDest": "_site/service-worker.js",
  "swSrc": "service-worker.js"
};