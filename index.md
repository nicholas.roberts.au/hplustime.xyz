---
layout: default.html
title: H+Time
tags: root
---

<h2>Recent posts</h2>
<ul class="posts">
{%- for post in collections.post -%}
  {% if process.env.NODE_ENV != "production" or post.data.draft != true %}
  <article{% if page.url == post.url %} class="post" aria-current="page"{% endif %}>
    <header>
      <h3>
        <a href={{ post.url }}>
          {{ post.data.title }}
        </a>
      </h3>
      <time datetime="{{ post.data.date | toUTCString }}">
        {{ post.data.date | formatDate }}
      </time>
    </header>
    <p class="excerpt">{{ post.data.excerpt }}</p>
  </article>
  {% endif %}
{%- endfor -%}
</ul>
<h2>Experiments</h2>
<ul class="posts">
{%- for post in collections.experiment -%}
  {% if process.env.NODE_ENV != "production" or post.data.draft != true %}
  
  <article{% if page.url == post.url %} class="post" aria-current="page"{% endif %}>
    <header>
      <h3>
        <a href={{ post.url }}>
          {{ post.data.title }}
        </a>
      </h3>
      <time datetime="{{ post.data.date | toUTCString }}">
        {{ post.data.date | formatDate }}
      </time>
    </header>
    <p class="excerpt">{{ post.data.excerpt }}</p>
  </article>
  {% endif %}
{%- endfor -%}
</ul>