const limits = [300, 780];
const maxDelta = limits[1] - limits[0];
const hSpectrum = [
    364.6,
    383.5397,
    388.9064,
    397.0075,
    410.1734,
    434.0472,
    486.135,
    656.279,
]

const spectrumValues = [
    `hwb(270 0% 75%)`,
    `hwb(260 0% 60%)`,
    `hwb(255 0% 50%)`,
    `hwb(250 0% 50%)`,
    `hwb(245 0% 0%)`,
    `hwb(237 0% 0%)`,
    `rgb(0, 239, 255)`,
    `hwb(0 100% 0%)`,
]

const settings = [
    {filter: 'blur(1px)', width: 1},
    {filter: null, width: 1},
    {filter: null, width: 1},
    {filter: null, width: 1},
    {filter: null, width: 1},
    {filter: null, width: 2,
        // shadowColor: 'hwb(237 0% 0%)', lineWidth: 1, strokeStyle: 'hwb(237 100% 0%)'
    },
    {filter: null, width: 4},
    {filter: `blur(0.5px)`, shadowColor: 'hwb(0 0% 0%)', shadowBlur: 10, width: 2, strokeStyle: 'hwb(0 0% 0%)', lineWidth: 3},
]

const epsilon = .0001;

const breaks = hSpectrum.map(_break => {
    const position = (_break - limits[0]) / maxDelta;
    return position;
});
registerPaint('emissionspectrum', class {
    static get contextOptions() {
        return {
            alpha: true,
            imageSmoothingQuality: 'high',
        }
    }
    paint(ctx, size, props) {
        ctx.imageSmoothingQuality = 'high';
        for(let [index, position] of breaks.entries()) {
            const currentSettings = settings[index];
            ctx.filter = settings[index].filter ?? '';
            ctx.fillStyle = spectrumValues[index];
            for (const key of ['shadowBlur', 'shadowColor', 'strokeStyle', 'lineWidth']) {
                ctx[key] = currentSettings[key] ?? '';
            }
            const startX = position * size.width;
            if(currentSettings['strokeStyle']) {
                ctx.beginPath();
                ctx.moveTo(startX + currentSettings.width / 2, 0);
                ctx.lineTo(startX + currentSettings.width / 2, size.height);
                ctx.stroke();
            }
            ctx.fillRect(startX, 0, settings[index].width, size.height);
        }
    }
});