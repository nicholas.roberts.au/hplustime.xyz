const litPlugin = require('@lit-labs/eleventy-plugin-lit');
const EleventyVitePlugin = require("@11ty/eleventy-plugin-vite");
const syntaxHighlighting = require('@11ty/eleventy-plugin-syntaxhighlight');
module.exports = (eleventyConfig) => {
    // eleventyConfig.addWatchTarget('./components');
    eleventyConfig.addPassthroughCopy('./components');
    eleventyConfig.addPassthroughCopy('./experiments');
    eleventyConfig.addPassthroughCopy({
        static: '/'
    });
    // eleventyConfig.addPlugin(litPlugin, {
    //     mode: 'worker',
    //     componentModules: [
    //         './components/demo-greeter.js',
    //     ],
    //     dir: {
    //         input: 'content',
    //         includes: '../_includes'
    //     }
    // });
    eleventyConfig.addPlugin(syntaxHighlighting);
    eleventyConfig.addPlugin(EleventyVitePlugin);
    eleventyConfig.addLiquidFilter('toUTCString', input => input.toUTCString())
    eleventyConfig.addLiquidFilter('formatDate', input => {
        return Intl.DateTimeFormat('en-US', { year: "numeric", month: "long", day: "numeric" }).format(input);
    })
}