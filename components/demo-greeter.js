import { LitElement, html, css } from 'lit';
import '@material/mwc-button';
import '@material/mwc-slider';
export class DemoGreeter extends LitElement {
    static properties = {
        hue: { type: Number },
        white: { type: Number },
        brightness: { type: Number }
    };

    static styles = css`
        :host {
            --hue: 0;
            --white: 0%;
            --brightness: 0%;
            margin-left: 20px;
            position: relative;
        }
        ::slotted(*) {
            margin-inline: 20px;
        }
        .hue-preview {
            background-color: hwb(var(--hue) var(--white) var(--brightness));
            width: 100px;
            height: 50px;
            margin-left: 50px;
        }
        mwc-slider {
            margin: revert;
        }
        .container {
            margin: 10px;
            display: grid;
            grid-template-columns: 1fr 3fr;
            place-items: center stretch;
            width: 300px;
        }
    `
    hueHandler = (event) => {
        this.hue = event.detail.value;
        this.style.setProperty('--hue', this.hue);
    }
    whiteHandler = (event) => {
        this.white = event.detail.value;
        this.style.setProperty('--white', CSS.percent(this.white))
    }
    brightnessHandler = (event) => {
        this.brightness = event.detail.value;
        this.style.setProperty('--brightness', CSS.percent(this.brightness))
    }
    render() {
        return html`
        <div class="container">
            <span>Hue</span>
            <mwc-slider min=0 max=360 value=${this.hue} @input=${this.hueHandler} @change=${this.hueHandler}></mwc-slider>
            <span>White Level</span>
            <mwc-slider min=0 max=100 value=${this.white} @input=${this.whiteHandler}></mwc-slider>
            <span>Brightness</span>
            <mwc-slider min=0 max=100 value=${this.brightness} @input=${this.brightnessHandler}></mwc-slider>
        </div>
        <div class="hue-preview"></div>
        `
    }
    connectedCallback() {
        super.connectedCallback();
        this.style.setProperty('--hue', CSS.number(this.hue ?? 0));
        this.style.setProperty('--white', CSS.percent(this.white ?? 0));
        this.style.setProperty('--brightness', CSS.percent(this.brightness ?? 0));
    }
}
customElements.define('demo-greeter', DemoGreeter)