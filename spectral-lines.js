const limits = [360, 780];
const maxDelta = limits[1] - limits[0];
const hSpectrum = [
    656.279,
    486.135,
    434.0472,
    410.1734,
    397.0075,
    388.9064,
    383.5397,
    364.6
].reverse();

const spectrumValues = [
    `rgb(255, 0, 0)`,
    `rgb(0, 239, 255)`,
    `rgb(40, 0, 255)`,
    `rgb(126, 0, 219)`,
    `rgb(129, 0, 169)`,
    `rgb(120, 0, 136)`,
    `rgb(109, 0, 115)`,
    `rgb(97, 0, 97)`
];



const epsilon = .0001;

const breaks = hSpectrum.map(_break => {
    const position = (_break - limits[0]) / maxDelta;
    return [position - epsilon, position + epsilon]
});
const formatter = new Intl.NumberFormat('en-US', {
    style: 'percent', maximumFractionDigits: 2,
    minimumFractionDigits: 2
})

const gradientEntries = [`to right, transparent ${formatter.format(breaks[0][0])}`];

for(let [index, pair] of breaks.entries()) {
    const lower = formatter.format(pair[0]);
    const upper = formatter.format(pair[1]);
    gradientEntries.push(
        `${spectrumValues[index]} ${lower} ${upper}`
    );
    const startOfNextLine = (breaks?.[index + 1]?.[0] ?? 1);
    const nextLower = formatter.format(startOfNextLine);
    gradientEntries.push(`transparent ${upper} ${nextLower}`);
}
console.log(gradientEntries.join(','))