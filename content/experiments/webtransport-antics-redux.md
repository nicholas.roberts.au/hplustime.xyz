---
title: WebTransport antics
tags: experiment
date: 2022-07-27
draft: true
layout: default.html
excerpt: A DeckGL visualization that offloads aggregated point cloud filtering to xarray via WebTransport
---
<script type="module" src='/experiments/webtransport-antics/index.js'>
</script>



<webtransport-client></webtransport-client>
