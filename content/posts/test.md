---
title: lit-element embeds in 11ty
layout: default.html
tags: post
date: 2022-05-07
---

## Why?
90% of a blog post is usually static markup, with the odd embedded image or snippet of diagramming/typesetting (e.g. mermaid, LaTeX). That last 10%, you have a few main options:
* Ditch the nice, static nature of the blog and do it up in a UI framework (blogs as SPAs are probably overkill)
* Embed a recording of the demo
* Actually deploy the thing in it's own page, link/iframe to it (high effort).

Since 11ty compiles out to html+css, and markdown inputs accept html tags, a fourth alternative exists:
WebComponent embeds (we're using lit-element here).
1. For blog-wide components, stick an `import(<components_root>/<component_path>.js)` in the template layout. This post uses this approach.
2. For one-offs, stick the import in the post document under a script tag.

The sample below is a quickly hacked together hwb color picker based on typed CSS properties.

<demo-greeter hue=231 white=33 brightness=0></demo-greeter>